---
tags: public, blog
---
<!--
SYNCTAG: do not edit in git-st, but in [codimd](https://md.inf.tu-dresden.de/blog-future-robotics-lab)
-->

# Blog of the BING Impulse Lab "Future Robotics"
* Zoom-Raum: https://kurzelinks.de/future-robotics-lab

## First Meeting 2023-02-06, 9:00-11:00, APB, E023

### Agenda
1. Vision Talk: "Development Process for the Future Robotics Lab (Structure, Topic Areas)
2. Themenfeld–Landkarte (Matrix Technologieforschungsfelder x Anwendungsfelder)
    * Definition von Themenfeldern (Kreuzungspunkte und -felder) für Großprojekt-Anträge bei 
        * DFG: GRK, FG, SFB: Für ein Themenfeld müssen Technologiefelder mit ein oder zwei Anwendungsfedern gekreuzt werden
        * Sachsen, BMBF, EU
5. Informationen über Akquisesituation 
    1. DFG, BMBF (N. Seifert)
    2. EU (N. N.)
    6. Innoteams
6. Einteilung der Arbeit an den Studienprofilen
7. Vorstellung im nächsten AK Robotics des Silicon Saxony

## Technologieforschungsfelder (Technology Research Fields) des Future Robotics Lab
mit Konferenzen und Zeitschriften/Journals
* Cobotics with Human in the loop (Thema von CeTI)
* Robot Swarms (Robotic Collectives)
    * Gemischte Schwärme aus Flug- und Fahrrobotern
* Telerobotics
* Outdoor Robotics
    * Underwater Robotics
    * Aerial Robotics
* Green Robotics: Schnelle und energiesparsame Hardware für Robotik  
    * FPGA in Drohnen
    * Silicon Nanowire Hardware für energiesparsame Sensorik 
* Non-von-Neumann Robotics
* Sniffbots
* Microrobotics
* Unterwasserrobotik


## Anwendungsfelder (Application Areas, Domains) des Future Robotics Lab
* Disaster Management / Katastrophenschutz-Robotik 
    * Sandsackwall-Bau 
    * Teleinspektion von Gaslecks 
* Rekultivierungsrobotik  
    * Teleinspektion von abrutschungsgefährdeten Flächen
* Recycling-Robotik  / Disassembly Robotics
    * Demontage von Autos
    * Demontage von Handys 
* Construction Robotics
    * Automatisierung von Baustellen
    * Teleinspektion auf Baustellen  
    * Plattenbau 4.0
* Farming Robotics
* Care Robotics
* Surgery Robotics
* Robotics in Education (RiE)
