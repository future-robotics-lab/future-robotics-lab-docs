# Members of Future Robotics Lab

| Prof.         |      | Thema                                           | email                                                       |
| ------------- | ---- | ----------------------------------------------- | ----------------------------------------------------------- |
| Beitelschmidt | MW   | Dynamik und Mechanismentechnik                  | Michael Beitelschmidt <michael.beitelschmidt@tu-dresden.de> |
| Herlizius     | MW   | Landmaschinen                                   | Thomas Herlizius  <thomas.herlitzius@tu-dresden.de>         |
| Schmidt       | MW   | Logistiksysteme                                 | Thorsten Schmidt <thorsten.schmidt@tu-dresden.de>           |
| Will          | MW   | Baumaschinen und -roboter                       | Frank Will  <frank.will@tu-dresden.de>                      |
| Cuniberti     | MW   | Sensorik und Materialien                        | Gianaurelio Cuniberti <gianaurelio.cuniberti@tu-dresden.de> |
| Ihlenfeld     | MW   | Industrierobotik                                | Steffen Ihlenfeldt <steffen.ihlenfeldt@tu-dresden.de>       |
| Felsmann      | MW   | Gebäudeenergietechnik und -wartung              | Clemens Felsmann <clemens.felsmann@tu-dresden.de>           |
| Schmauder     | MW   | Kollaborative Robotik                           | Martin Schmauder <martin.schmauder@tu-dresden.de>           |
| Krzywinski    | MW   | Technisches Design                              | Jens Krzywinski <jens.krzywinski@tu-dresden.de>             |
| Fitzek        | ETIT | 6G communications für Cobotik                   | Frank Fitzek <frank.fitzek@tu-dresden.de>                   |
| Fettweis      | ETIT | 6G communications für Robotik                   | Gerhard Fettweis <Gerhard.Fettweis@vodafone-chair.com>      |
| Tetzlaff      | ETIT | Non-von-Neumann Hardware für Robotik            | Ronald Tetzlaff <ronald.tetzlaff@tu-dresden.de>             |
| Zerna         | ETIT | Zentrum für mikrotechnische Produktion          | Thomas Zerna <thomas.zerna@tu-dresden.de>                   |
| Janschek      | ETIT | Automation Engineering                          | Klaus Janschek  <klaus.janschek@tu-dresden.de>              |
| Richter       | ETIT | Institut für Halbleiter- und Mikrosystemtechnik | Andreas Richter <andreas.richter7@tu-dresden.de>            |
| Pfifer        | MW   | Professur für Flugmechanik und Flugregelung     | Harald Pfifer <harald.pfifer@tu-dresden.de>                 |
| Goehringer    | INF  | Adaptive Dynamische Systeme                     | Diana Goehringer <diana.goehringer@tu-dresden.de>           |
| Sommer        | INF  | Prozessmodellierung                             | Sommer, Christoph <christoph.sommer@tu-dresden.de>          |
| Aßmann        | INF  | Softwaretechnologie                             | Uwe Assmann <uwe.assmann@tu-dresden.de>                     |
| Fetzer        | INF  | Systems Engineering                             | Christof Fetzer <christof.fetzer@tu-dresden.de>             |
| Schirmeier    | INF  | Betriebssysteme                                 | Horst Schirmeier <horst.schirmeier@tu-dresden.de>           |
| Köpsell       | INF  | Privacy and Security                            | Stefan Köpsell  <stefan.koepsell@tu-dresden.de>             |
| Wollschlaeger | INF  | Industrial Informatics                          | Martin Wollschlaeger <Martin.Wollschlaeger@tu-dresden.de>   |
| Dachselt      | INF  | Multimediatechnik                               | Raimund Dachselt <raimund.dachselt@tu-dresden.de>           |
| Gumhold       | INF  | Computergrafik                                  | Stefan Gumhold <stefan.gumhold@tu-dresden.de>               |
| McGinity      | INF  | Immersive Medien in der Robotik                 | Matthew McGinity <matthew.mcginity@tu-dresden.de>           |
| Blüher        | BING | Referentin Interd. Forschung                    | Anja Blüher  <anja.blueher@tu-dresden.de>                   |
| Seifert       | BING | Projekt Scout                                   | Nicolle Seifert <Nicolle.Seifert@tu-dresden.de>             |
|               |      |                                                 |                                                             |

