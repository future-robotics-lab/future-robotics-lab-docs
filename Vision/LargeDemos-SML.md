# Large Robotic Demonstrators in SML

* Uwe Aßmann, Version 0.1, 2024-10-24

## Telerobotics

* Teleinspection
  * Sniff-Swarms for Inspection
* Tele-maintenance

## Outdoor Robotics

* Automated flood wall construction
* Automated mesh-joinery bridge production
* Drone fleets for infrastructure maintenance
  * Drone fleet for STRASSE
