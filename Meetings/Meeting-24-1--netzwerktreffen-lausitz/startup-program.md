# Pitch-Session am 3. Netzwerktreffen Lausitz, April 15, 2024, 13:00

* Uwe Aßmann, Version 0.1, 2024-04-10

## Ort

Dülfersaal, TU Dresden, https://www.google.de/maps/place/D%C3%BClferstra%C3%9Fe,+01069+Dresden-Plauen/@51.0267126,13.7246043,17z/data=!4m15!1m8!3m7!1s0x4709c594aeb99da5:0x9aa33cd77f5fe9b4!2sD%C3%BClferstra%C3%9Fe,+01069+Dresden-Plauen!3b1!8m2!3d51.0267126!4d13.7271792!16s%2Fg%2F1td4hv4q!3m5!1s0x4709c594aeb99da5:0x9aa33cd77f5fe9b4!8m2!3d51.0267126!4d13.7271792!16s%2Fg%2F1td4hv4q?entry=ttu

## Teilnehmer in Reihenfolge

* Maximilian Meiler, Meiler, Maximilian <maximilian.meiler@tu-dresden.de> Lars Hornuf  <lars.hornuf@tu-dresden.de> (Financial Robotics)
* Niclas Trelle <niclas.trelle@mailbox.tu-dresden.de> (TRID Systems, https://trid-systems.com/)
* Nils Twelker <nils.twelker@mailbox.tu-dresden.de> (XR immersive home, https://immersive-home.org/)
* Ariel Podlubne <ariel.podlubne@tu-dresden.de> (Smarobix, https://smarobix.com/)
* David Uebel <uebel@quantumgradematerials.de> (Quantum Grade Materials, https://www.niederlausitz-aktuell.de/niederlausitz-aktuell/orte/cottbus/238712/quantencomputing-technologie-siegt-bei-letzter-lex-pitchrunde-2023.html)

## Ablauf des Pitch-Slots der Startups (20min)

* 5 Min Vorstellung Geschäftsmodell des Startups oder Prä-Startups
* 5 Min Vorstellung, wie man eine Infrastruktur der Lausitzprojekte nutzen würde (Smart Mobility Lab, CircEcon, etc, auch von der BTU)
* 10 Min Kommentare und Anregungen der Juroren und der Verantwortlichen für die Lausitzprojekte. Bitte beachten: da nur 10 Min Zeit bleiben, haben wir dieses Mal nicht zuviele Juroren. Für jeden Juror sind ca. 3min eingeplant. 

## Öffentlichkeitsarbeit

Die Startups haben die Möglichkeit, eine halbe Seite (mit Foto) als "Startup of the Day" auf der Webseite des Future Robotic Labs bzw. des BING zu gestalten. Dazu bitte Text und Foto an Frau Jacqueline Duwe von der Öffentlichkeitsarbiet senden:  <jacqueline.duwe@tu-dresden.de>
