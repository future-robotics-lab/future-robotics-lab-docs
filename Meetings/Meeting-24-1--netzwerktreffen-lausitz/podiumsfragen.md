# Fragen an Podiumsteilnehmer des 3. Netzwerktreffens Lausitz (15.4.2024, 15:15)

* Uwe Aßmann, Version 0.1, 2024-04-04

Broschüre der TU Dresden für die Forschungsinfrastruktur-Projekte in der Lausitz: https://flipbook.webspace.tu-dresden.de/lausitz-campus/index.html

Lausitzprojekte der BTU Cottbus-Senftenberg: https://www.b-tu.de/universitaet/partnerschaften-strukturwandel/strukturwandel-lausitz 

Moderation: Prof. Uwe Aßmann, Softwaretechnologie, TU Dresden http://www1.inf.tu-dresden.de/~ua1/

## Fragen

* Was sind wichtige Schritte, um in den nächsten Jahren durch dei Forschungsinfrastruktur-Projekte in der Lausitz Wissenstransfer zu erzeugen und Startups zu generieren?
  * Für die Hochschulen
  * Für die Inkubationseinrichtungen der Länder (TGFS, etc.)
  * Für die Professoren?
* Welche Motivationssysteme könnte man für die Beteiligten installieren?
  * Wie sollte eine Hochschule für ein Startup belohnt werden?
* Welche Förderinstrumente wären sinnvoll einzusetzen?
  * Innoteams
  * EU-Förderung
  * DFG-Förderung
* Wie beurteilen Sie die Möglichkeiten der Forschungsinfrastrukturen in der Lausitz für die "Reifungsphase" der Technologieforschung ("Validierung des Innovationspotentials/VIP")? https://www.foerderdatenbank.de/FDB/Content/DE/Foerderprogramm/Bund/BMBF/validierung-des-technologischen-und-gesellschaftl.html
  * In wie weit würden sich Länder-Förderverfahren für VIP lohnen?
* Wie beurteilen Sie die Chancen von "Entrepreneurial Masters"?
* Welche  "Entrepreneurial Masters" wären für die Lausitz passend?
* Wie kann man "Entrepreneurial Masters" mit nachhaltiger Startup-Generierung verknüpfen?
