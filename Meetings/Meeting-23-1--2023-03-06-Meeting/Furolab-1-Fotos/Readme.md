# Explanation to PostIts

* Fotos in folder Matrix-Technologies-Applications show competences in the matrix of technologies x application areas. 
    * Colors in Matrix-Technologies-Applications:
        * green: running or pöast projects (competences)
        * yellow: ideas for the future
* Fotos in folder Matrix-Technologies-Applications show **values (pains or gains)** of humans, economical, societal, or environmental pains or gains:
  * Colors in Matrix-Technologies-Applications-Pains-Gains:
    * Blue: Pains
    * Orange: Gains
* Fotos in Folder SpecificTechnologies sum up competences in specific crosscutting technologies.
