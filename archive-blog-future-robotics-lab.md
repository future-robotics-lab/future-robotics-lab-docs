---
tags: public, blog
---


:::info
SYNCTAG: edit in [hedgedoc](https://md.inf.tu-dresden.de/notes/blog-future-robotics-lab)
:::

# Blog of the BING Impulse Lab "Future Robotics"
* Mailingliste <bing-robotic-lab@groups.tu-dresden.de>
* [Zoom-Raum für Teilname an hybriden Meetings](https://kurzelinks.de/future-robotics-lab)
* [AK Robotics des Silicon Saxony](https://www.silicon-saxony.de/leistungen/arbeitskreise/advanced-automation-robotics/)
*  Special Interest Working Groups (SIG)
    * SIG Outdoor robotics (construction robotics, farm robotics, recycling, recultivation)
    * SIG Industrial robotics
    * SIG Aerial/Space robotics
    * SIG Microrobotics
    * SIG ROS
* Ringvorlesung for WS 23/24?
* Website

#### Documents
* [gitlab Directory in Chemnitz with lab-public documents](https://gitlab.hrz.tu-chemnitz.de/future-robotics-lab/future-robotics-lab-docs)
* [Shared space on cloudstore](https://cloudstore.zih.tu-dresden.de/index.php/s/677BdAn3KpsSBCY)
* [Vision des Future Robotics Lab](https://gitlab.hrz.tu-chemnitz.de/future-robotics-lab/future-robotics-lab-docs/-/blob/main/2023-01-25-vision-furolab.md) (2023-01-25) | [Vision Slide Set](https://gitlab.hrz.tu-chemnitz.de/future-robotics-lab/future-robotics-lab-docs/-/blob/main/2023-01-25-introduction-future-robotics-lab.pdf)
* [Mission des Future Robotics Lab](https://gitlab.hrz.tu-chemnitz.de/future-robotics-lab/future-robotics-lab-docs/-/blob/main/2023-01-25-mission-future-robotics-lab.md) (2023-01-25)

## Current List of Articles for Press Presentation
- Construction Robotics, a chance for Saxony
    - Robotics in Construction industry (C. Hahn/Cherif)
    - WallBot-Masonry Robot presented at MAUMA 2022 (Will)
    - CARE
    - A great challenge: Assembly of Maxoli stones
- More Senses to the Cobot!
    - Digitizing Touch (R. Calandra)
    - Sniffing Dangerous Gases in desasters like in the Ahrtal  2021 (U. Aßmann)
- Virtual Worlds for Robots (M. McGinity)
- Feldschwarm-Projekt "Rubin" (T. Herlizius)
- Robots Commissioning Tools in Metal Forming (IHL/Peuler), SPP 2422

#### Meeting 6: 2024-Juni?
Demo/Impulse Prof. Calandra

#### Meeting 5: 2024-März?
Demo/Impulse: Prof. Ihlenfeld, Dr. Penter

##### Preliminary Agenda
* Introduction to the proposal writing for DFG Graduiertenkollegs (Aßmann, DFG-FK 409)


## Meeting 4: 3rd Lausitz-Netzwerktreffen “Wirtschaft trifft Wissenschaft” (tent. Apr 15, 2024 Dülfersaal) with topic “Chancen der Robotik der Zukunft”
* Reihe der Netzwerktreffen: https://tu-dresden.de/ing/forschung/konferenzreihe-wirtschaft-trifft-wissenschaft
* Preliminary Agenda: https://md.inf.tu-dresden.de/notes/witriwi-lausitz-netzwerktreffen3
* Smart Mobility Lab https://flipbook.webspace.tu-dresden.de/lausitz-campus/index.html 

We are looking for contributions:
* Demos: 
    * VR-Presentation of the Smart Mobility Lab in Hoyerswerda, see (Aßmann)
    * Roboterschwarm "Max und Moritz" (Aßmann)
    * Sniffbot-Demo: Der Roboter, der giftige Gase schnüffeln kann (Bereich ING)
* Posters:

## Meeting 23-3: Friday, 2023-09-08, 9:00-11:00 
Host: Prof. Thomas Herlizius, Farm Robotics; Prof. Will, Baurobotik
Place:  [Institut für Naturstofftechnik](https://tu-dresden.de/ing/maschinenwesen/int/das-institut/Anfahrt)
Protocol:  https://datashare.tu-dresden.de/index.php/f/27153740
Profs. Frank Will   and Thomas Herlitzius are inviting for a demonstration of physical examples of outdoor robotics application, which are under development at the chairs Agriculture Systems and Technology and Construction Machinery. Showcases are:
* Feldschwarm and its mobile 5G campus net infrastructure
* Vineyard and Orchard robot
* Scouting robot
* Robotic carrier for masonry construction
* Tracked platform for robotic applications
* Conventional (human operated) tractor and harvesting combined

#### Agenda:
- Informations about Meeting 4 at Nov.14, 2023, Dülfersaal (Aßmann) (5min)
    - will be combined with  the 3rd Lausitz-Netzwerktreffen "Wirtschaft trifft Wissenschaft" with topic "Chancen der Robotik der Zukunft", see https://tu-dresden.de/ing/forschung/konferenzreihe-wirtschaft-trifft-wissenschaft
    - Presentation (demos, posters) opportunities 
- Introduction and insights into the general use cases of robotic applications in Ag (Herlitzius) and Construction (Will) / 30 min
- Breakdown in 3-4 groups to see, smell and feel the machines / 60 min
- Joint final discussion and taking a ride on tractor and combine for the bravest among us / 30-60 min


#### Results:
* Agricultural robotics is an industrial market ever increasing. 
* Compensation of deformations, vibration damping
* Many are concerned about ROS 1+2
    * Workshop TUD-ROS 
    * Wallbot, Elwobot, Feldschwarm
    * Mover demo (Will)
    * Elwobot demo (Herlitzius)
* MA-Liste, PHD Liste
* stepwise virtual prototyping
* reliability
* ROS workshop is to be organized



## Zweites Treffen 2023-05-05, 8:30-10:30
INF Ratssaal
Protocol: https://datashare.tu-dresden.de/s/FFB6Bwp4R5LZk63
* The decision was taken to form 4 special interest groups (SIG), out of the clusters of the topic areas (technologies x applications) brainstorming of the first meeting:
    * SIG Outdoor robotics (construction robotics, farm robotics, recycling, recultivation)
    * SIG Industrial robotics
    * SIG Aerial/Space robotics
    * SIG Microrobotics
    * SIG ROS
    * We will collect memberships of these SIG before the third meeting.
* Fotos of the Brainstorming results at the Value Proposition Canvases
* We have an offer of R. Calandra to chair a colloquium series (Kolloquiums-Reihe) „AI and Robotics“ (Chair R. Calandra)
    * Can we organize a Ringvorlesung for WS 23/24?

## Erstes Treffen (Kickoff) 2023-03-06, 9:00-11:00, ZEU 252
Protocol: https://datashare.tu-dresden.de/s/DQtScG5XAWczCQq
### Agenda
1. Vision Talk: "Development Process for the Future Robotics Lab (Structure, Topic Areas) (U. Aßmann, 15min)
2. Web-Auftritt des Future Robotics Lab
3. **Themenfeld–Landkarte** (Matrix Technologieforschungsfelder x Anwendungsfelder)
    * Definition von Themenfeldern (Kreuzungspunkte und -felder) für Großprojekt-Anträge bei 
        * DFG: GRK, FG, SFB: Für ein Themenfeld müssen Technologiefelder mit ein oder zwei Anwendungsfedern gekreuzt werden
        * Sachsen, BMBF, EU
    * Positionierung der KollegInnen an dem Whiteboard
    * Einteilung von Special Interest Groups (SIG), die online-Treffen organisieren, alternierend mit den Haupttreffen
4. Informationen über **Akquisesituation** 
    1. DFG 
    2. [BMBF](https://cloudstore.zih.tu-dresden.de/index.php/s/99z3XjA4KEPPEWY) (N. Seifert)
    3. EU (N. N.)
    6. Innoteam2 
5. Einteilung der Arbeit an den **Studienprofilen**, Sammlung von möglichen Kursen, auch Kreuz-Import/Export
    7. INF-DSE-Profil "Software for Future Robotics"
    8. MW-Mechatronik-Profil 
    9. Andere
6. Sammlung von Themen und Demos
    * Vorstellung im nächsten [AK Robotics des Silicon Saxony](https://www.silicon-saxony.de/leistungen/arbeitskreise/advanced-automation-robotics/)
    * Zeitungsbeilage "Future Robotics" für Anfang 2024
7. Verschiedenes
    * Termine

## Technologieforschungsfelder (Technology Research Fields) des Future Robotics Lab
mit Konferenzen und Zeitschriften/Journals
* Cobotics with Human in the loop (Thema von CeTI)
* Robot Swarms (Robotic Collectives)
    * Gemischte Schwärme aus Flug- und Fahrrobotern
* Telerobotics
* Outdoor Robotics
    * Underwater Robotics
    * Aerial Robotics
* Green Robotics: Schnelle und energiesparsame Hardware für Robotik  
    * FPGA in Drohnen
    * Silicon Nanowire Hardware für energiesparsame Sensorik 
* Non-von-Neumann Robotics
* Sniffbots
* Microrobotics
* Unterwasserrobotik
* Digital Twins for robotics

## Anwendungsfelder (Application Areas, Domains) des Future Robotics Lab
* Disaster Management / Katastrophenschutz-Robotik 
    * Sandsackwall-Bau 
    * Teleinspektion von Gaslecks 
* Rekultivierungsrobotik  
    * Teleinspektion von abrutschungsgefährdeten Flächen
* Recycling-Robotik  / Disassembly Robotics
    * Demontage von Autos
    * Demontage von Handys 
* Construction Robotics
    * Automatisierung von Baustellen
    * Teleinspektion auf Baustellen  
    * Plattenbau 4.0
* Farming Robotics (Feldschwarm)
* Care Robotics
* Surgery Robotics
* Robotics in Education (RiE)

## Kandidaten für Special Interest Groups als Themenfelder der Matrix (Technologieforschungsfelder x Anwendungsfelder)
Diese Liste kann beliebig erweitert werden:
* *Unterwasserschnüffel-Robotik:* Prof. Cuniberti's Carbon-Nano-Sensoren können Stoffe im Wasser erschnüffeln. Wie können wir einen Unterwasser-Schnüffelroboter entwickeln, der giftige Stoffe in Flüssen, in Seen oder im Meehr findet? Die Gewässer nach Stoffen kartiert?

## Regelmässige Agendapunkte für das Plenum
* Vision film studio: wir sehen uns gemeinsam Visionäre Filme an wie Microsoft Future Productivity Vision
* Visionsdokumente VMOSA für die Technologiefelder
    * Sammlung von Fallstudien
    * Erstellung von Visionspapieren "Problems, Visions and Challenges"
* Visionsdokumente VMOSA für die Technologiefelder
* Edit eines Special Issues auf nationaler Ebene (Informatik-Spektrum o.ä.)


## Akquise-Informationen
(bitte ergänzen)
### DFG-Maßnahmen
#### Forschungsgruppen FOR
* Modellbasiertes Opportunistisches Roboterplanen für Mensch-Roboter-Kollaboration (Alexandra Kirsch) 2012 bis 2017 
    * Das Ziel des MORPH Projektes ist es, das Konzept von günstigen Gelegenheiten zu untersuchen, um Roboterverhalten effizienter und für Menschen verständlicher zu machen
* Anticipating Human Behavior FOR 2535 Professor Dr. Jürgen Gall, Bonn
    * https://gepris.dfg.de/gepris/projekt/313421352 
* Antizipative Mensch-Roboter-Kollaboration (P8) (Sven Behnke) Teilprojekt zu FOR 2535 2017 bis 2021
* IP1: Einbeziehung kurzfristiger räumlich-zeitlicher Informationen für die Robotersensorik Antragsteller Professor Dr. Christopher McCool seit 2022
    * https://gepris.dfg.de/gepris/projekt/498557852
* In FOR https://gepris.dfg.de/gepris/projekt/459376902 
* FOR 1513:  Hybrides Schließen in intelligenten Systemen (2012 bis 2019) 
* https://gepris.dfg.de/gepris/projekt/167839951 
FOR 1321:  Single-Port-Technologie für gastroenterologische und viszeralchirurgische endoskopische Interventionen (2011-2020)
https://gepris.dfg.de/gepris/projekt/136437427	

![](https://md.inf.tu-dresden.de/uploads/upload_16b152e59d186a9cc0967bf58dfd3d92.png)
