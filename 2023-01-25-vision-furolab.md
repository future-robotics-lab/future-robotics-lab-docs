# Vision for the Future Robotics Lab (FuRoLab) in the School of Engineering (BING) of TU Dresden

Pre-Founding Draft
Prof. Dr. Uwe Aßmann, Version 0.4, 25. Jan. 2023

*Editorial Remark: Draft.* The vision document is meant to present the vision of the Furolab in all faculties. This document will be re-worked together after the founding of the BING Impulse Lab. 

## Vision for Society

In 2040, many robotic products, services, and solutions will serve our societies worldwide.  Robots will revolutionize our world and automate many more tasks of our daily life than today. It is the mission of the Future Robotics Lab to research methods, technology, and applications for this robotic revolution. The robotic revolution will be driven by factors spanning up three dimensions: business models, technology, and application areas. 

New robot technology will form a first dimension of the future application of robots. Robots will collaborate with humans (*Cobotics*), will be operating in groups (*Swarm Robotics*), and self-re-load their energy from autonomous loading at batteries or green methanole, or green hydrogen stations (*Green Robotics*). Robots will be sniffing by employing modern sniffing sensors (*Sniffbots*). Non-von-Neumann Robots will be built with innovative Hardware and Software Architectures, which will render the robots much more resilient and flexible than today. 

New business models for robotics will dominate the application markets. Therefore, the business model dimension will highly influence the future application markets of robots (Fig. 1). New robotic products will serve individual persons (*Personal Robotics*), but robotic services will also provided by shared infrastructure in a share-economy (*Robotics as a Service, RaaS*), as well as bought in pay-per-use (*Pay-per use Robotics*). 

Finally, new applications in many application areas such as industry, farming, disaster management, will have started to flourish, and these application fiels form the third dimension of the robotic world in 2040 (Robotics in Application Fields).

In 2030, the future robotic lab of TU Dresden will have explored many case studies of this vision, filling the white spots in the 3-dimensional diagram of Fig. 1, leading to new innovations in technology transfer, to push Saxony into a pole position for the future robotic world of 2040. 

Be part of it by joining the Future Robotics Lab!

![image-20230125164118199](https://gitlab.hrz.tu-chemnitz.de/future-robotics-lab/future-robotics-lab-docs/-/blob/main/skizze-3d.png)


Figure: The central research themes of Future Robotics Lab, arranged in 3 dimensions.

## New Business Models

### Personal Robotics and Social Robotics

Due to the decline of prices for robotic hardware, in 2040 many robots for personal use will be offered in the markets. Personal robotic psyiotherapy, immersive healthcare, house cleaning beyond vacuum cleaning, robotic cooking services, or clean-up robots for rooms are only a few applications to name. Additionally, robotic and cobotic apps will be downloadable from special app stores with safety-certified apps. Because these apps offer offer safety and product liability, and the certification will stay a cost-intensive process, these apps will cost a fee and can generate value. Social robotics extends personal robotics to processes in the society. 

### Robotic services and solutions (Robotics as a Service, RaaS)

In 2040, robots and robotic services will be available for rent and shared in a share-economy. Many robots for specific purposes will still be too expensive for personal use, but may provide a shared infrastructure, so that share-economy business models are appropriate (technology as a service). Therefore, companies will be founded that offer Robotics as a Service (RaaS). 

### Robotics with Pay-per-use

If robots form a public infrastructure, the RaaS business model will be not appropriate, instead, a pay-per-use model will be the best to employ. Why not have a car parked by the robots of the downtown parking lot, paying a Euro by mobile phone? 

## Technology fields for Research

### Cobotics

Robots will collaborate with humans, protecting their safety and guessing how to help them (attentive assistance). It will be as natural to collaborate with a cobot as working with an animal or a human collaborator. 

### Swarm Robotics

While many robotic products and services will rely on standalone robots, in 2040 many services will be run by robotic swarms that can be managed from central operator dashboards, combine tasks on land, air and sea and are able to meet and collaborate with other robotic swarms. Robotic swarms are self-integrating systems being able to adapt to new complex tasks while collaborating with other robotic swarms. We envision that some robot swarms can also be rented as a service, and that companies will exist that rent out robot swarms. 

### Green Robotics

In the future, robotics should be completely fossil-free, i.e., green. Robots are autonomous or semi-autonomous systems that will be able to care for their energy needs autonomously. Innovative forms of batteries for regenerative energy (Hydrogen, Natrium, rust, liquid air) shall be applied on different levels of scale. Battery change, resupply, or charging will be performed autonomously. Robotic services will be cooperating locally, demand-driven, and scheduled for optimized safety, robustness, and energy consumption. 
## Robotic Solutions in Application Fields

In 2040, many new markets for robots will have started to flourish.

* In farming, field health will be monitored precisely for precision farming. 
* In logistics, autonomous transport robot swarm applications as well as serious drone applications, such as transport of important medicaments, goods, or documents, will grow in number. With such logistics mobility services, not just the last mile but also the pre-last mile of fast transport services can be optimized. 
* In disaster management, robot swarms and robots-as-a-service will play a major role, for disaster predition (forest fire and flood prognosis), disaster management (fire fighting, flood protection). 
* In industrial production, cobots will play a major role, robots that protect and collaborate with human workers. 
* In healthcare, robots will take over assistive care tasks.

This is an open-ended list.

The Future Robotics Lab will work for the crosscutting topic areas of technology fields, application fields, and business models.
