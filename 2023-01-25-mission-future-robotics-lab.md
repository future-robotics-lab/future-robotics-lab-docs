# Mission for the Future Robotics Lab (FuRoLab) in the School of Engineering (BING) of TU Dresden
Pre-Founding Draft
Prof. Dr. Uwe Aßmann, Version 0.6, 25. Jan. 2023 - first md version

*Editorial Remark: Draft.* This mission document is meant to present the vision of the Furolab in all faculties. This document will be re-worked together after the founding of the BING Impulse Lab. 

## The Mission of Future Robotics Lab

It is the mission of the Future Robotics Lab (FuRoLab) of TU Dresden to bundle all competences for robotics in the school of engineering (BING) of TU Dresden. 

### Education Mission

- Build up new study program profiles in existing Master programs:
  - MW-MA Mechatronik (MW): Profile "Mechatronics for Robotics" 
  - INF-MA Distributed Systems Engineering (INF): Profile "Software Engineering for Robots"
  - Plan topics for Blockpraktika into the "Smart Mobility Lab in Hoyerswerda"
- Support the existing robotics courses
  - Lego Mindstorms
    - INF (Fetzer)
    - MW (Beitelschmidt)
  - Others

### Research Mission

-  Organize impulse talks of members
-  Collaborate on common DFG clusters
-  Plan research demonstrations in the "Smart Mobility Lab in Hoyerswerda"
-  Research on new topics such as recycling and recultivation robotics, precision farming, construction and reconstruction robotics, factory in a box

### Dissemination Mission

* Produce once a year a press article about own work
* Organize a youtube film platform

### Networking Mission

* Collaborate with the working group "robotics" of Silicon Saxony
